-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2020 at 06:25 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `databaseland`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_time`
--

CREATE TABLE `book_time` (
  `Login_ID` varchar(15) NOT NULL,
  `Rides_ID` varchar(5) NOT NULL,
  `Date` date NOT NULL,
  `Time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `consist_of`
--

CREATE TABLE `consist_of` (
  `Rides_ID` varchar(5) NOT NULL,
  `Ticket_ID` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `Login_ID` varchar(15) NOT NULL,
  `Fname` varchar(100) DEFAULT NULL,
  `Lname` varchar(100) DEFAULT NULL,
  `Passwords` varchar(20) DEFAULT NULL,
  `Date_Of_Birth` date DEFAULT NULL,
  `Gender` varchar(1) DEFAULT NULL,
  `E_mail` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `period_time`
--

CREATE TABLE `period_time` (
  `Rides_ID` varchar(5) NOT NULL,
  `Period_Time` varchar(20) NOT NULL,
  `Date` date NOT NULL,
  `Number_Of_Customers` int(11) DEFAULT NULL,
  `Status` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rides`
--

CREATE TABLE `rides` (
  `Rides_ID` varchar(5) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Type` varchar(1) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Capacity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `Ticket_ID` varchar(5) NOT NULL,
  `Name` varchar(20) DEFAULT NULL,
  `Price` int(11) DEFAULT NULL,
  `Number_Of_Round` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `Trans_ID` varchar(6) NOT NULL,
  `Login_ID` varchar(15) DEFAULT NULL,
  `Ticket_ID` varchar(5) DEFAULT NULL,
  `Date_Of_Entrance` date DEFAULT NULL,
  `Date_Of_Purchase` date DEFAULT NULL,
  `PaymentType` varchar(1) DEFAULT NULL,
  `Receipt` varchar(15) DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `QR_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_time`
--
ALTER TABLE `book_time`
  ADD PRIMARY KEY (`Login_ID`,`Rides_ID`,`Date`,`Time`),
  ADD KEY `Rides_ID` (`Rides_ID`);

--
-- Indexes for table `consist_of`
--
ALTER TABLE `consist_of`
  ADD PRIMARY KEY (`Rides_ID`,`Ticket_ID`),
  ADD KEY `Ticket_ID` (`Ticket_ID`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`Login_ID`);

--
-- Indexes for table `period_time`
--
ALTER TABLE `period_time`
  ADD PRIMARY KEY (`Rides_ID`,`Period_Time`,`Date`);

--
-- Indexes for table `rides`
--
ALTER TABLE `rides`
  ADD PRIMARY KEY (`Rides_ID`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`Ticket_ID`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`Trans_ID`),
  ADD KEY `Login_ID` (`Login_ID`),
  ADD KEY `Ticket_ID` (`Ticket_ID`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `book_time`
--
ALTER TABLE `book_time`
  ADD CONSTRAINT `book_time_ibfk_1` FOREIGN KEY (`Login_ID`) REFERENCES `member` (`Login_ID`),
  ADD CONSTRAINT `book_time_ibfk_2` FOREIGN KEY (`Rides_ID`) REFERENCES `rides` (`Rides_ID`);

--
-- Constraints for table `consist_of`
--
ALTER TABLE `consist_of`
  ADD CONSTRAINT `consist_of_ibfk_1` FOREIGN KEY (`Rides_ID`) REFERENCES `rides` (`Rides_ID`),
  ADD CONSTRAINT `consist_of_ibfk_2` FOREIGN KEY (`Ticket_ID`) REFERENCES `ticket` (`Ticket_ID`);

--
-- Constraints for table `period_time`
--
ALTER TABLE `period_time`
  ADD CONSTRAINT `period_time_ibfk_1` FOREIGN KEY (`Rides_ID`) REFERENCES `rides` (`Rides_ID`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`Login_ID`) REFERENCES `member` (`Login_ID`),
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`Ticket_ID`) REFERENCES `ticket` (`Ticket_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
