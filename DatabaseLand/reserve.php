<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking Time-DatabaseLand</title>
    <!-- Bootstrap core CSS -->
    <link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Clicker+Script&family=Sofia&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="datetimepicker-master/build/jquery.datetimepicker.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
   <script>
                        $('select').selectpicker();
                        $(function () {
                            $('.selectpicker').selectpicker();
                        });

                    </script>

    <style>
        a {
            text-decoration: none;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
</head>

<body>
    <header>

        <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #f7e37f;">
            <a class="navbar-brand" href="#"><img src="img/IMG_0427.PNG" width="30" height="30"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">

                <a href="index.php">
                    <li class="navbar-brand" style="font-family: 'Clicker Script', cursive; font-weight: bolder;">
                        DatabaseLand</li>
                </a>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="index.php"><svg class="bi bi-house" width="1em" height="1em"
                                viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z"
                                    clip-rule="evenodd" />
                                <path fill-rule="evenodd"
                                    d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z"
                                    clip-rule="evenodd" />
                            </svg> Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ticket.php"><i class="fa fa-shopping-basket"></i> Buy Ticket</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="Booktime.php"><svg class="bi bi-calendar" width="1em" height="1em"
                                viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"
                                    clip-rule="evenodd" />
                                <path fill-rule="evenodd"
                                    d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z"
                                    clip-rule="evenodd" />
                            </svg> Booking Time</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Qr.php"><i class="fa fa-qrcode"></i> QR code</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link btn btn-warning" href="signout.php"><svg class="bi bi-person" width="1em"
                                height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M13 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM3.022 13h9.956a.274.274 0 00.014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 00.022.004zm9.974.056v-.002.002zM8 7a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0z"
                                    clip-rule="evenodd" />
                            </svg> Sign out</a>
                    </li>
                </ul>
                
            </div>
        </nav>


    </header>
    <body>
<?php
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
   include("connect.php");
   if(isset($_POST['submit'])){
       $rideid =$_POST['rideid'];
       $loginid = $_POST['loginid']; 
       $date = $_POST['date'];
       $quantity = $_POST['q']; 
       $rname = $_POST['rm'];
       $_SESSION["TransID"] = $_POST['tranid'];
       foreach($_POST['time'] as $pt){
        
        $checkr = $mysql-> query("SELECT Rides_ID,Time FROM book_time WHERE Date='{$date}' AND Rides_ID='{$rideid}'");
        $numr = mysqli_num_rows($checkr);
        $check = $mysql-> query("SELECT Time FROM book_time WHERE Date='{$date}' AND Time='{$pt}'");
        $num =  mysqli_num_rows($check);
        //check duplicate time
            if($num>0){
                echo "<script>
            setTimeout(function() {
            swal({
            title: \"This time was resered\",
            text: \"Please go back and select another round\",
            type: \"error\",
            showCancelButton: false,
            confirmButtonColor: \"#DD6B55\",
            confirmButtonText: \"Go back\",
            
            closeOnClickOutside: false,
        }, function(isConfirm) {
            if(isConfirm){
                window.history.back();}
            
        });
        
    }, 1000);
        </script>"; 
            }
            //update time ride
            elseif($numr > 0){
                $delRide = $checkr -> fetch_assoc();
                if($pt != "-"){
                        $mysql->query("UPDATE book_time
                        SET Time = '{$pt}'
                        WHERE Rides_ID = '{$rideid}' 
                        AND Date = '{$date}' AND Login_ID = '{$loginid}'
                        AND Time = '{$delRide['Time']}'");
                        echo "<script>
                        setTimeout(function() {
                        swal({
                        title: \"".$rname."'s round was update\",
                        text: \"update from ".$delRide['Time']." to ".$pt."\",
                        type: \"success\",
                        showCancelButton: false,
                        confirmButtonColor: \"#33D11E\",
                        confirmButtonText: \"Go back\",
                
                        closeOnClickOutside: false,
                        }, function(isConfirm) {
                        if(isConfirm){
                             window.history.back();}
                
                            });
            
                         }, 1000);
                         </script>"; }
                else{
                   
                    //Delete form book time
                    $mysql->query("DELETE FROM  book_time 
                    WHERE Login_ID = '{$loginid}' 
                    AND Rides_ID = '{$rideid}' 
                    AND Date = '{$date}'");

                    // $mysql->query("UPDATE book_time
                    // SET Time = '{$pt}'
                    // WHERE Login_ID = '{$loginid}' 
                    // AND Rides_ID = '{$rideid}' 
                    // AND Date = '{$date}'");
                    //update after delete
                    $mysql->query("UPDATE Period_Time
                    SET Number_Of_Customers = Number_Of_Customers - '{$quantity}'
                    WHERE Rides_ID = '{$rideid}' 
                    AND Date = '{$date}' 
                    AND Period_Time = '{$delRide['Time']}'");
                    echo "<script>
                    setTimeout(function() {
                    swal({
                    title: \"".$rname."'s round was cancel\",
                    text: \"go back and select other round\",
                    type: \"success\",
                    showCancelButton: false,
                    confirmButtonColor: \"#33D11E\",
                    confirmButtonText: \"Go back\",
            
                    closeOnClickOutside: false,
                    }, function(isConfirm) {
                    if(isConfirm){
                         window.history.back();}
            
                        });
        
                     }, 1000);
                     </script>"; 
                    }
                }


            
        else{
        
            if($pt != "-"){ 
                $mysql->query("INSERT INTO book_time (Login_ID,Rides_ID,Date,Time)
                VALUES ('{$loginid}', '{$rideid}', '{$date}', '{$pt}')");
                $mysql->query("UPDATE Period_Time
                SET Number_Of_Customers = Number_Of_Customers+ '".$quantity."'
                WHERE Rides_ID = '".$rideid."' 
                AND Date = '".$date."' AND Period_Time = '".$pt."'");}
                echo "<script>
                setTimeout(function() {
                swal({
                title: \"".$rname."'s round was reserved successfully\",
                text: \"go back and select other round\",
                type: \"success\",
                showCancelButton: false,
                confirmButtonColor: \"#33D11E\",
                confirmButtonText: \"Go back\",
        
                closeOnClickOutside: false,
                }, function(isConfirm) {
                if(isConfirm){
                window.history.back();}
        
            });
    
            }, 1000);
            </script>"; 
    }
        
       // echo $rideid.$loginid.$date.$pt;
        
       } 

   }
   elseif(isset($_POST['submitm'])){
    $rideid = $_POST['rideid'];
    $loginid = $_POST['loginid']; 
    $date = $_POST['date'];
    $quantity = $_POST['q']; 
    $rname = $_POST['rm'];
    $_SESSION["TransID"] = $_POST['tranid'];
    foreach($_POST['time'] as $pt){
     
     $checkr = $mysql-> query("SELECT Rides_ID,Time FROM book_time WHERE Date='{$date}' AND Rides_ID='{$rideid}'");
     $numr = mysqli_num_rows($checkr);
     $check = $mysql-> query("SELECT Time,Rides_ID FROM book_time WHERE Date='{$date}' AND Time='{$pt}'");
     $num =  mysqli_num_rows($check);
     //check duplicate time
         if($num>0){
            //  $n=0;
            //  while($s = $check->fetch_assoc()){
            //      if($s['Rides_ID'] == $rideid){
            //          $n+=1;
            //      }
            //  }
            //  if($n!=0){
            //      continue;
            //  }

             echo "<script>
         setTimeout(function() {
         swal({
         title: \"This time was resered\",
         text: \"".$pt." was reserved,Please go back and select another round\",
         type: \"error\",
         showCancelButton: false,
         confirmButtonColor: \"#DD6B55\",
         confirmButtonText: \"Go back\",
         
         closeOnClickOutside: false,
     }, function(isConfirm) {
         if(isConfirm){
             window.history.back();}
         
     });
     
 }, 1000);
     </script>"; 
         }
         //update time ride
         elseif($numr > 0){
             
             if($pt == "-"){
                 while($delRide = $checkr -> fetch_assoc()){
                      //Delete form book time
                 $mysql->query("DELETE FROM  book_time 
                 WHERE Login_ID = '{$loginid}' 
                 AND Rides_ID = '{$rideid}' 
                 AND Date = '{$date}'
                 AND Time = '{$delRide['Time']}'");
                
                 //update after delete
                 $mysql->query("UPDATE Period_Time
                 SET Number_Of_Customers = Number_Of_Customers - '{$quantity}'
                 WHERE Rides_ID = '{$rideid}' 
                 AND Date = '{$date}' 
                 AND Period_Time = '{$delRide['Time']}'");
                 }
                 echo "<script>
                 setTimeout(function() {
                 swal({
                 title: \"".$rname."'s round was cancel\",
                 text: \"go back and select other round\",
                 type: \"success\",
                 showCancelButton: false,
                 confirmButtonColor: \"#33D11E\",
                 confirmButtonText: \"Go back\",
         
                 closeOnClickOutside: false,
                 }, function(isConfirm) {
                 if(isConfirm){
                      window.history.back();}
                     });
     
                  }, 1000);
                  </script>"; 
                }
                else{
                    $mysql->query("INSERT INTO book_time (Login_ID,Rides_ID,Date,Time)
                    VALUES ('{$loginid}', '{$rideid}', '{$date}', '{$pt}')");
                    $mysql->query("UPDATE Period_Time
                    SET Number_Of_Customers = Number_Of_Customers+ '".$quantity."'
                    WHERE Rides_ID = '".$rideid."' 
                    AND Date = '".$date."' AND Period_Time = '".$pt."'");
                 echo "<script>
                 setTimeout(function() {
                 swal({
                 title: \"".$rname."'s round was reserved successfully\",
                 text: \"go back and select other round\",
                 type: \"success\",
                 showCancelButton: false,
                 confirmButtonColor: \"#33D11E\",
                 confirmButtonText: \"Go back\",
                 
                 closeOnClickOutside: false,
             }, function(isConfirm) {
                 if(isConfirm){
                     window.history.back();}
                 
             });
             
            }, 1000);
             </script>"; 

                }
      
             }

         
     else{
            if($pt != "-"){
                $mysql->query("INSERT INTO book_time (Login_ID,Rides_ID,Date,Time)
                VALUES ('{$loginid}', '{$rideid}', '{$date}', '{$pt}')");
                $mysql->query("UPDATE Period_Time
                SET Number_Of_Customers = Number_Of_Customers+ '".$quantity."'
                WHERE Rides_ID = '".$rideid."' 
                AND Date = '".$date."' AND Period_Time = '".$pt."'");}
                echo "<script>
                setTimeout(function() {
                swal({
                title: \"".$rname."'s round was reserved successfully\",
                text: \"go back and select other round\",
                type: \"success\",
                showCancelButton: false,
                confirmButtonColor: \"#33D11E\",
                confirmButtonText: \"Go back\",
     
                closeOnClickOutside: false,
                }, function(isConfirm) {
                if(isConfirm){
                window.history.back();}
     
                });
 
                }, 1000);
                </script>"; 
 }
     
    // echo $rideid.$loginid.$date.$pt;
     
    } 
}
?>
</body>
</html>