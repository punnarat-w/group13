<?php
    session_start();
    include('updateperiodtime.php');
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Home-DatabaseLand</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/carousel/">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Clicker+Script&family=Pacifico&family=Sofia&display=swap"
        rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">


    <style>
        body {
            position: relative;
        }

        a {
            text-decoration: none;
        }
    </style>

    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">

</head>

<body>
<!-- if login already -->
<?php if(isset($_SESSION["uid"])) { ?>
    <header>

        <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #f7e37f;">
            <a class="navbar-brand" href="#"><img src="img/IMG_0427.PNG" width="30" height="30"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">

                <a href="index.php">
                    <li class="navbar-brand" style="font-family: 'Clicker Script', cursive; font-weight: bolder;">
                        DatabaseLand</li>
                </a>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><svg class="bi bi-house" width="1em" height="1em"
                                viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z"
                                    clip-rule="evenodd" />
                                <path fill-rule="evenodd"
                                    d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z"
                                    clip-rule="evenodd" />
                            </svg> Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ticket.php"><i class="fa fa-shopping-basket"></i> Buy Ticket</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Booktime.php"><svg class="bi bi-calendar" width="1em" height="1em"
                                viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"
                                    clip-rule="evenodd" />
                                <path fill-rule="evenodd"
                                    d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z"
                                    clip-rule="evenodd" />
                            </svg> Booking Time</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Qr.php"><i class="fa fa-qrcode"></i> QR code</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link btn btn-warning" href="signout.php"><svg class="bi bi-person" width="1em"
                                height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M13 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM3.022 13h9.956a.274.274 0 00.014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 00.022.004zm9.974.056v-.002.002zM8 7a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0z"
                                    clip-rule="evenodd" />
                            </svg> Sign out</a>
                    </li>
                </ul>
              
            </div>
        </nav>


    </header>
   
<!-- else login yet         -->
<?php } else { ?>
    <header>

<nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #f7e37f;">
    <a class="navbar-brand" href="#"><img src="img/IMG_0427.PNG" width="30" height="30"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">

        <a href="index.php">
            <li class="navbar-brand" style="font-family: 'Clicker Script', cursive; font-weight: bolder;">
                DatabaseLand</li>
        </a>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php"><svg class="bi bi-house" width="1em" height="1em"
                        viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z"
                            clip-rule="evenodd" />
                        <path fill-rule="evenodd"
                            d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z"
                            clip-rule="evenodd" />
                    </svg> Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="signin.php"><i class="fa fa-shopping-basket"></i> Buy Ticket</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="signin.php"><svg class="bi bi-calendar" width="1em" height="1em"
                        viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"
                            clip-rule="evenodd" />
                        <path fill-rule="evenodd"
                            d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z"
                            clip-rule="evenodd" />
                    </svg> Booking Time</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="signin.php"><i class="fa fa-qrcode"></i> QR code</a>
            </li>

            <li class="nav-item active">
                <a class="nav-link btn btn-warning" href="signin.php"><svg class="bi bi-person" width="1em"
                        height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M13 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM3.022 13h9.956a.274.274 0 00.014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 00.022.004zm9.974.056v-.002.002zM8 7a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0z"
                            clip-rule="evenodd" />
                    </svg> Sign in</a>
            </li>
        </ul>
      
    </div>
</nav>

<?php } ?>
</header>



<div id="myCarousel" class="carousel slide" data-ride="carousel">
<ul class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <!-- <li data-target="#myCarousel" data-slide-to="2"></li> -->
</ul>
<div class="carousel-inner">
    <div class="carousel-item active">
        <img class="d-block w-100" src="img/d2.jpg" alt="First slide">

        <div class="container">
            <div class="carousel-caption text-left">
                <h1 style="font-family: 'Pacifico', cursive;">World of Happiness</h1>
                <p>Just an hour and a half from Bangkok, you will get to DatabaseLand <br>
                    The “World of Happiness” has rides and fun activities for everyone.</p>
                <p><a class="btn btn-lg btn-primary" href="signup.php" role="button"
                        style="background-color:#F67664  ; border-color: #F67664  ;">Sign up today</a></p>
            </div>
        </div>
    </div>
    <div class="carousel-item ">
        <img class="d-block w-100" src="img/d3.jpg" alt="Second slide">

        <div class="container">
            <div class="carousel-caption">
                <h1 style="font-family: 'Pacifico', cursive;">Fall back to your dream</h1>
                <p>With over 40 thrill and gentle rides, DatabaseLand lets you live your childhood dream.
                </p>
                <p><a class="btn btn-lg btn-primary" href="signup.php" role="button"
                        style="background-color: #F67664  ; border-color: #F67664  ;">Sign up today</a></p>
            </div>
        </div>
    </div>

</div>
<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>
</div>


<main role="main">
<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing">

    <!-- Three columns of text below the carousel -->

    <div class="row">
        <div class="col-lg-4">
            <image src="img/IMG_0441.png" class="rounded-circle" width="140" height="140" alt=""></image>
            <h2>EXCITING ZONE</h2>
            <p>The Thailand's newest and the most exciting adventure park
                by the virtue of its size and capacity providing amazingly
                fun games and activities;
                some of them ofter the highest level of adrenalin pumping excitement.</p>
            <p><button class="btn btn-secondary" id="ex">View details &raquo;</button></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <image src="img/IMG_0438.png" class="rounded-circle" width="140" height="140" alt=""></image>
            <h2>KID ZONE</h2>
            <p>The center of the family. Here at DatabaseLand,we gather variety of different fun games for your
                whole family to spend a good memorial time together.</p>
            <p><button class="btn btn-secondary" id="kid">View details &raquo;</button></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <image src="img/IMG_0442.png" class="rounded-circle" width="140" height="140" alt=""></image>
            <h2>WATER ZONE</h2>
            <p>DatabaseLand guarantees the fun and joyful for the whole family with non-stop water play action
                on the multi-level platforms </p>
            <p><button class="btn btn-secondary" id="wa">View details &raquo;</button></p>
        </div><!-- /.col-lg-4 -->

    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <a href="signin.php"><button type="button" class="btn btn-warning btn-lg btn-block">Enter to the
                    Happiness</button></a>

        </div>
        <div class="col-lg-4"></div>
    </div>





</div><!-- /.container -->


<!-- FOOTER -->
<footer class="container">
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>&copy; 2017-2019 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
</footer>
</main>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
<script src="/docs/4.4/dist/js/bootstrap.bundle.min.js"
integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm"
crossorigin="anonymous"></script>

<!-- The Modal -->
<div id="myModal" class="modal">
<!-- Modal content -->
<div class="modal-content">
    <div class="modal-header">

        <h2 style="font-family: 'Clicker Script', cursive; font-weight: bolder; text-align: center;">Welcome to
            DatabaseLand!!!</h2>
        <span class="close" id="close">&times;</span>
    </div>

    <img src="img/IMG_0428.JPG" style="width: 100%;height: 100%;">
    <div class="modal-footer">
        <button id="bt" type="button" class="btn btn-danger btn-lg btn-block">Enter Website</button>
    </div>
</div>
</div>
<div id="myModalex" class="modal2">
<!-- Modal content -->
<div class="modal-content">
    <div class="modal-header">

        <h2 >EXCITING ZONE</h2>
        <p>The Thailand's newest and the most exciting adventure park
            by the virtue of its size and capacity providing amazingly
            fun games and activities <br>
            some of them ofter the highest level of adrenalin pumping excitement.</p>
        <span class="close" id="x">&times;</span>
    </div>

    <img src="img/IMG_0443.PNG" style="width: 100%;height: 100%;">
    <div class="modal-footer">
        <button id="bt2" type="button" class="btn btn-danger btn-lg btn-block">Close</button>
    </div>
</div>
</div>
<div id="myModalkid" class="modal2">
<!-- Modal content -->
<div class="modal-content">
    <div class="modal-header">

        <h2 >KID ZONE</h2>
        <p>The center of the family. Here at DatabaseLand,we gather variety of different fun games for your
            whole family to spend a good memorial time together.</p>
        <span class="close" id="y">&times;</span>
    </div>

    <img src="img/IMG_0435.PNG" style="width: 100%;height: 100%;">
    <div class="modal-footer">
        <button id="bt3" type="button" class="btn btn-danger btn-lg btn-block">Close</button>
    </div>
</div>
</div>
<div id="myModalwa" class="modal2">
<!-- Modal content -->
<div class="modal-content">
    <div class="modal-header">

        <h2 >WATER ZONE</h2>
        <p>DatabaseLand guarantees the fun and joyful for the whole family with non-stop water play <br> action
            on the multi-level platforms. </p>
        <span class="close" id="z">&times;</span>
    </div>

    <img src="img/IMG_0447.PNG" style="width: 100%;height: 100%;">
    <div class="modal-footer">
        <button id="bt4" type="button" class="btn btn-danger btn-lg btn-block">Close</button>
    </div>
</div>
</div>

<script>
var modal = document.getElementById("myModal");
var modalex = document.getElementById("myModalex");
var modalk = document.getElementById("myModalkid");
var modalw = document.getElementById("myModalwa");
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    else if (event.target == modalex) {
        modalex.style.display = "none";
    }
    else if (event.target == modalk) {
        modalk.style.display = "none";
    }
    else if (event.target == modalw){
        modalw.style.display = "none";
    }
}

</script>
<script>
// Get the modal
var modal = document.getElementById("myModal");
var btc = document.getElementById("bt");
// Get the <span> element that closes the modal
var span = document.getElementById("close");
btc.onclick = function () {
    modal.style.display = "none";
}
 // When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
// window.onclick = function (event) {
//     if (event.target == modal) {
//         modal.style.display = "none";
//     }
// }
</script>
<script>
// Get the modal
var modalex = document.getElementById("myModalex");
var idex = document.getElementById("ex");
var btc = document.getElementById("bt2");
// Get the <span> element that closes the modal
    var span = document.getElementById("x");
btc.onclick = function () {
    modalex.style.display = "none";
}
// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modalex.style.display = "none";
}

idex.onclick = function () {
    modalex.style.display = "block";
}
</script>
<script>
// Get the modal
var modalk = document.getElementById("myModalkid");
var idkid = document.getElementById("kid");
var btc = document.getElementById("bt3");
// Get the <span> element that closes the modal
var span = document.getElementById("y");
btc.onclick = function () {
    modalk.style.display = "none";
}
// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modalk.style.display = "none";
}

idkid.onclick = function () {
    modalk.style.display = "block";
}
</script>
<script>
// Get the modal
var modalw = document.getElementById("myModalwa");
var idwa = document.getElementById("wa");
var btc = document.getElementById("bt4");
// Get the <span> element that closes the modal
var span = document.getElementById("z");
btc.onclick = function () {
    modalw.style.display = "none";
}
// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modalw.style.display = "none";
}

idwa.onclick = function () {
    modalw.style.display = "block";}

</script>

</body>


</html>