<?php
    session_start();
    $error = false;
    $error_message = "";
    //checklogin
    if(isset($_SESSION["uid"])) header('Location: index.php');
    //create variable
    include('connect.php');
    //check post method variable
    if(isset($_POST['loginid'])&&$_POST['password'])
    {
        //echo $_POST['loginid']."   ".$_POST['password'];
        $user = $_POST['loginid'];
        $pwd = $_POST['password'];
        // echo $user." ".$pwd;
        $stmt = $mysql->prepare("SELECT Login_ID FROM member WHERE Login_ID = ? AND passwords = ?");

        $stmt->bind_param('ss',$user,$pwd);

        $stmt->execute();

        $result = $stmt->get_result();

        // print_r($result);

        if($uid = $result->fetch_assoc())
        {
            $_SESSION["uid"] = $uid['Login_ID'];
            header('Location: index.php');
        }
        else
        {
            $error = true;
            $error_message = "Wrong password or Login_ID please try again";
            // header('Location: /Group13/signin.php');
        }       
    }
    // else
    // {
    //     echo "need loginid and password";
    // }
?>
    
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign In-DatabaseLand</title>
    <!-- Bootstrap core CSS -->
    <link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Clicker+Script&family=Pacifico&family=Sofia&display=swap" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">


    <style>
        a {
            text-decoration: none;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
</head>

<body>
        <header>
            
            <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #f7e37f;">
                <a class="navbar-brand" href="#"><img src="img/IMG_0427.PNG" width="30" height="30"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                
                        <a href="index.php">
                            <li class="navbar-brand" style="font-family: 'Clicker Script', cursive; font-weight: bolder;">DatabaseLand</li>
                        </a>
                        <ul class="navbar-nav ml-auto"> 
                        <li class="nav-item ">
                            <a class="nav-link" href="index.php"><svg class="bi bi-house" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
                                <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z" clip-rule="evenodd"/>
                            </svg> Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="ticket.php"><i class="fa fa-shopping-basket"></i> Buy Ticket</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Booktime.php"><svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>
                                <path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>
                            </svg> Booking Time</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="Qr.php"><i class="fa fa-qrcode"></i> QR code</a>
                        </li>
                
                        <li class="nav-item active">
                            <a class="nav-link btn btn-warning" href="signin.php"><svg class="bi bi-person" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M13 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM3.022 13h9.956a.274.274 0 00.014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 00.022.004zm9.974.056v-.002.002zM8 7a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0z" clip-rule="evenodd"/>
                            </svg> Sign in</a>
                        </li>
                    </ul>
                
                </div>
            </nav>
        
            
        </header>
        

        <main role="main">
            <div class="container marketing">

                <div class="row" style="margin-top: 5%;">

                    <div class="col-md-8">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <ul class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <!-- <li data-target="#myCarousel" data-slide-to="2"></li> -->
                            </ul>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="img/d1.jpg" alt="First slide">

                                    <div class="container">
                                        <div class="carousel-caption text-left">
                                            <h1 style="font-family: 'Pacifico', cursive;">World of Happiness</h1>
                                            <p>Just an hour and a half from Bangkok, you will get to DatabaseLand <br>
                                                The “World of Happiness” has rides and fun activities for everyone.</p>
                                            <p><a class="btn btn-lg btn-primary" href="signup.php" role="button"
                                                    style="background-color: #F27C14  ; border-color: #F27C14  ;">Sign up today</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item ">
                                    <img class="d-block w-100" src="img/d6.jpg" alt="Second slide">

                                    <div class="container">
                                        <div class="carousel-caption">
                                            <h1 style="font-family: 'Pacifico', cursive;">Fall back to your dream</h1>
                                            <p>With over 40 thrill and gentle rides, DatabaseLand lets you live your
                                                childhood dream.
                                            </p>
                                            <p><a class="btn btn-lg btn-primary" href="signup.php" role="button"
                                                    style="background-color: #F27C14  ; border-color:#F27C14 ;">Sign up today</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4"><br><br>
                        <h1 style="font-family: 'Sofia', cursive;">Sign In<img src="img/cake.png" width="40" height="40"></h1>
                        <form action="signin.php" method="post">
                            <div class="form-group">
                                <label for="loginid">Login ID</label>
                                <input type="loginid" class="form-control" id="loginid" name="loginid" placeholder="Login ID" 
                                required pattern="[A-Za-z0-9]*" minlength="6" maxlength="15">

                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password"
                                required pattern="[A-Za-z0-9]*" minlength="8" maxlength="20">
                            </div>
                            <div class="form-group ">
                                <a href="forgetpass.php"><small id="emailHelp" class="form-text text-muted">Forget password</small></a>
                            </div>
                            <input type="submit" class="btn btn-info bt-lg btn-block"><br>
                        
                        </form>
                        <small class="form-text text-muted">Don't have any account? Let's sign up!</small>
                        <a href="signup.php"><button type="submit" class="btn btn-outline-info bt-lg btn-block">Sign
                                Up</button></a>
                    </div>

                </div>

            </div>



            <!-- FOOTER -->
            <footer class="container">
                <p class="float-right"><a href="#">Back to top</a></p>
                <p>&copy; 2017-2019 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
            </footer>
        </main>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
        <script src="/docs/4.4/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm"
            crossorigin="anonymous"></script>

</body>
<script>
    <?php if($error) { ?>
        swal("Error", "<?php echo $error_message; ?>", "error");
    <?php } ?>
</script>
</html>