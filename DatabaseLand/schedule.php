<?php 
 session_start();
 //include('checklogin.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Schedule-DatabaseLand</title>
    <!-- Bootstrap core CSS -->
    <link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Clicker+Script&family=Sofia&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="datetimepicker-master/build/jquery.datetimepicker.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    



    <style>
        a {
            text-decoration: none;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
</head>

<body>
    <header>
    <?php if(isset($_SESSION["uid"])) { ?>
        <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #f7e37f;">
            <a class="navbar-brand" href="#"><img src="img/IMG_0427.PNG" width="30" height="30"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">

                <a href="index.php">
                    <li class="navbar-brand" style="font-family: 'Clicker Script', cursive; font-weight: bolder;">
                        DatabaseLand</li>
                </a>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="index.php"><svg class="bi bi-house" width="1em" height="1em"
                                viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z"
                                    clip-rule="evenodd" />
                                <path fill-rule="evenodd"
                                    d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z"
                                    clip-rule="evenodd" />
                            </svg> Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ticket.php"><i class="fa fa-shopping-basket"></i> Buy Ticket</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="Booktime.php"><svg class="bi bi-calendar" width="1em" height="1em"
                                viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"
                                    clip-rule="evenodd" />
                                <path fill-rule="evenodd"
                                    d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z"
                                    clip-rule="evenodd" />
                            </svg> Booking Time</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Qr.php"><i class="fa fa-qrcode"></i> QR code</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link btn btn-warning" href="signout.php"><svg class="bi bi-person" width="1em"
                                height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M13 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM3.022 13h9.956a.274.274 0 00.014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 00.022.004zm9.974.056v-.002.002zM8 7a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0z"
                                    clip-rule="evenodd" />
                            </svg> Sign out</a>
                    </li>
                </ul>
              
            </div>
        </nav>


    </header>


    <main role="main">
        <div class="container marketing">

            <div class="row" style="margin-top: 5%;">

                <div class="col-md-4"><br><br><br><br><br><br><br><br><br><br>
                    <img src="img/GRIZZLY_GOD.png" width="300">
                </div>
                <div class="col-md-8"><br><br>
                    <h1 style="font-family: 'Sofia', cursive;">My Schedule<img src="img/S__884749.jpg" width="50">
                    </h1><br>
                    <?php 
                        include('checksc.php');
                        while($date = $sdate -> fetch_assoc()){
                            $ticket = $mysql->query("SELECT Ticket_ID,Trans_ID 
                            FROM Transactions
                            WHERE Login_ID = '{$loginid}'
                            AND Date_Of_Entrance = '{$date['Date']}'");                        
                            while($t = $ticket -> fetch_assoc()){
                                if( $t['Ticket_ID'] == "00001"){
                                    echo "<h5>Your ticket is Ticket A: Easy Pass</h5>";
                                }
                                elseif($t['Ticket_ID'] == "00002"){
                                    echo "<h5>Your ticket is Ticket B: Limited Pass</h5>";
                                }
                                elseif($t['Ticket_ID'] == "00003"){
                                    echo "<h5>Your ticket is Ticket C: Visa Pass</h5>";
                                }
                                else{
                                    echo "<h5>Your ticket is Ticket D: UnLimited Pass</h5>";
                                }
                                echo "<h6>Date:".$date['Date']."</h6>";

                                echo "<table class=\"table table-hover\">";
                                echo "<thead>";
                                echo  "<tr >";
                                echo  "<th scope=\"col\">Time</th>";
                                echo  "<th scope=\"col\">Rides</th>";
                                echo "</tr>";
                                echo "</thead>";
                                echo "<tbody>";
                                $i=0;
                                $ssc = $mysql->query("SELECT Book_Time.Time, Rides.Name
                                FROM Book_Time
                                INNER JOIN Rides
                                ON Book_Time.Rides_ID = Rides.Rides_ID
                                WHERE Book_Time.Date ='{$date['Date']}' AND Book_Time.Login_ID = '{$loginid}'
                                ORDER BY Book_Time.Date DESC , Book_Time.Time ASC");
                                while($sc = $ssc -> fetch_assoc()){
                                    if($i == 0){
                                        $i +=1;
                                        echo "<tr class=\"table-info\">";
                                        echo    "<td>".$sc['Time']."</td>";
                                        echo   "<td>".$sc['Name']."</td>";
                                        echo "</tr>";
                                    }
                                    else{
                                        $i -=1;
                                        echo "<tr >";
                                        echo    "<td>".$sc['Time']."</td>";
                                        echo   "<td>".$sc['Name']."</td>";
                                        echo "</tr>";
                                    }
                                }
                                echo "</tbody>";
                                echo "</table>";
                                echo "<div class=\"col-md-8\">";
                                echo   "<div class=\"row\">";
                                echo      "<div class=\"col-md-6\">";
                                echo           "<form action=\"Qr.php\" method=\"post\">
                                                <input type=\"hidden\" name=\"qloginid\" id=\"qloginid\" value=\"".$loginid."\">
                                                <input type=\"hidden\" name=\"qdate\" id=\"qdate\" value=\"".$date['Date']."\">
                                                <input type=\"hidden\" name=\"qticket\" id=\"qticket\" value=\"".$t['Ticket_ID']."\">
                                                <button type=\"submit\" class=\"btn btn-info bt-lg btn-block\" >My QR Code</button></form>";
                                echo        "</div>";
                                echo "<div class=\"col-md-6\">
                                        <form action=\"Booktime.php\" method=\"get\">
                                                <input type=\"hidden\" name=\"transedit\" id=\"transedit\" value=\"".$t['Trans_ID']."\">
                                                

                                            <button type=\"submit\" class=\"btn btn-dark bt-lg btn-block\" >Edit My Schedule</button></form>
                                             <br>
                                            </div>
                                        </div>
                                    </div>";
                            }
                        }
                    
                    ?>
                   <!-- <input type=\"hidden\" name=\"dated\" id=\"dated\" value=\"".$date['Date']."\">
                                                <input type=\"hidden\" name=\"ticket\" id=\"ticket\" value=\"".$t['Ticket_ID']."\">
                     -->
                      <!-- <div class="col-md-8">
                          <div class="row">
                              <div class="col-md-6">
                        <a href="Qr.html"><button type="submit" class="btn btn-info bt-lg btn-block" >My QR Code</button></a>
                    </div>
                    <div class="col-md-6">
                        <a href="Booktime.html"><button type="submit" class="btn btn-dark bt-lg btn-block" >Edit My Schedule</button></a><br>
                    </div>
                    </div>
                    </div> -->
                </div>
                

            </div>

        </div>



        <!-- FOOTER -->
        <footer class="container">
            <p class="float-right"><a href="#">Back to top</a></p>
            <p>&copy; 2017-2019 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
        </footer>
    </main>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="/docs/4.4/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm"
        crossorigin="anonymous"></script>
    <script src="datetimepicker-master/build/jquery.datetimepicker.full.min.js"></script>
    <script>
        $('#picker').datetimepicker({
            timepicker: false,
            datepicker: true,
            format: 'Y-m-d',
            value: '2020-4-8',
            weeks: true
        })
    </script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<!-- else login yet         -->
<?php } else { ?>
    <script>
        location.href = "signin.php"
    </script>
<?php } ?>
</body>

</html>